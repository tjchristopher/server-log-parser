class ServerLogParser
    attr_reader :views
    attr_reader :log_path
    def initialize(log_path)
        @log_path = log_path
        @views = Hash.new {|hash, key| hash[key] = []}
    end

    def parse
            File.open(@log_path).each do |line|
                page = line.split(/\s/)[0]
                ip = line.split(/\s/)[1]
                @views[page] << ip
            end
    end

    def most_views
        most_views = @views.sort_by { |k, v| v.length }.reverse unless @views.empty?
        most_views.each  { |k,v| puts  "#{k} has #{v.length} views" }
        most_views.to_h
    end

    def unique_views
        unique_views = @views.sort_by { |k, v| v.uniq!.length }.reverse unless @views.empty?
        unique_views.each { |k,v| puts "#{k} has #{v.length} unique views" }
        unique_views.to_h
    end

end