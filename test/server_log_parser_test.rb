require 'minitest/autorun'
require 'test_helper'
require_relative '../lib/server_log_parser.rb'

class ServerLogParserTest < Minitest::Test
    
    def setup
        @path = 'webserver.log'
    end
    
    def test_initialization
        parser = ServerLogParser.new(@path)
        assert parser.class == ServerLogParser
        assert parser.log_path == 'webserver.log'
        assert parser.views.class == Hash
    end

    def test_list_pages
        parser = ServerLogParser.new(@path)
        parser.parse
        assert parser.most_views.class == Hash
        assert parser.unique_views.class == Hash
    end

    def test_most_views_order
        parser = ServerLogParser.new(@path)
        parser.parse
        size = 0
        parser.most_views.each_value do |v|
            assert v.size > size
        end
    end

    def test_unique_views_order
        parser = ServerLogParser.new(@path)
        parser.parse
        size = 0
        parser.unique_views.each_value do |v|
            assert v.size > size
        end
    end
    
end